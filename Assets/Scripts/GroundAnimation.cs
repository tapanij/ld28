﻿using UnityEngine;
using System.Collections;

public class GroundAnimation : MonoBehaviour
{
		private int width;
		private int height;
		private float offsetX;
		private float offsetY;
		private float groundScale = 20f;
		private Texture2D texture;
		public Texture2D originalTexture;
		private SpriteRenderer spriteRend;
		private Vector2 pivotPt;
		private float[] widthSpeedModifiers;
		public bool startTextureAnimation = false;
		private float animationTime = 0f;
		public GameObject player;
		private float footstepCounter = 0f;
		private float footstepCounterStart = 0.2f;
		private bool leftFoot = false;

		// Use this for initialization
		void Start ()
		{
				footstepCounter = footstepCounterStart;

				pivotPt = new Vector2 (0.5f, 0.5f);
				spriteRend = GetComponent<SpriteRenderer> ();

				width = originalTexture.width;
				height = originalTexture.height;
				offsetX = width / 2f;
				offsetY = height / 2f;
		
				widthSpeedModifiers = new float[width];
				for (var i = 0; i < width; i++) {
						widthSpeedModifiers [i] = Random.Range (1f, 3f);
				}
		
				texture = new Texture2D (originalTexture.width, originalTexture.height, TextureFormat.ARGB32, false);
				texture.filterMode = FilterMode.Point;

				// Get the pixel block and place it into a new texture.
				var pix = originalTexture.GetPixels (0, 0, width, height);
				texture.SetPixels (pix);
				texture.Apply ();
		}
	
		// Update is called once per frame
		void FixedUpdate ()
		{
				if (startTextureAnimation) {
						Calculate ();
				}
		}

		void Calculate ()
		{
//				for (int y = 0; y<height; y++) {
//						for (int x = 0; x<width; x++) {
//
//								animationTime += Time.deltaTime;
////
////								float timeModifier = animationTime * y * 0.0005f;
//
////								Color px = originalTexture.GetPixel ((int)(x * inputX), (int)(y * inputY));
//								Color px = new Color (player.rigidbody2D.velocity.x, player.rigidbody2D.velocity.y, 0, 1);
//
////								float timeModifier = animationTime * y * 0.0005f;
////								Color px = originalTexture.GetPixel (x, (int)(y + (timeModifier * widthSpeedModifiers [x])));
//
//								texture.SetPixel (x, y, px);
//
//						}	
//				}

//				Color px = new Color (player.rigidbody2D.velocity.x, player.rigidbody2D.velocity.y, 0, 1);
				
				
				if (footstepCounter <= 0f) {

						float offX = offsetX;
						float offY = offsetY;
						if (leftFoot) {
								offX += 1f;
								offY -= 1f;
						}

						int x = (int)((player.transform.position.x * groundScale) + offX);
						int y = (int)((player.transform.position.y * groundScale - 6f) + offY);

						Color px = originalTexture.GetPixel (x, y);
						px.r -= 0.2f;
						px.g -= 0.2f;
						px.b -= 0.2f;

						texture.SetPixel (x, y, px);
						footstepCounter = footstepCounterStart;
						leftFoot = !leftFoot;
				}
		
				texture.Apply ();

				spriteRend.sprite = Sprite.Create (texture, new Rect (0, 0, width, height), pivotPt, 100f);

				footstepCounter -= Time.deltaTime;
		}
}
