// This script is placed in public domain. The author takes no responsibility for any possible harm.

//var gray = true;
private var width;
private var height;
//
//var lacunarity = 6.18;
//var h = 0.69;
//var octaves = 8.379;
//var offset = 0.75;
//var scale = 0.09;
//
//var offsetPos = 0.0;

private var texture : Texture2D;
public var originalTexture : Texture2D;
//public var originalMaterial: Material;
//private var perlin : Perlin;
//private var fractal : FractalNoise;
private var spriteRend : SpriteRenderer;
private var pivotPt : Vector2;
private var px : Color;
private var widthSpeedModifiers : float[];

function Start ()
{

	pivotPt = new Vector2(0.5f, 0.5f);
	spriteRend = GetComponent(SpriteRenderer);
	
	width = originalTexture.width;
	height = originalTexture.height;
	
	widthSpeedModifiers = new float[width];
	for(var i = 0; i < width; i++){
		widthSpeedModifiers[i] = Random.Range(1f, 3f);
	}

	texture = new Texture2D(originalTexture.width, originalTexture.height,  TextureFormat.ARGB32, false);
//	originalTexture = new Texture2D.
//	 renderer.material.mainTexture;

//	originalMaterial = Instantiate (renderer.material) as Material;
//	Debug.Log(originalMaterial.mainTexure);
//	originalMaterial.mainTexure = Instantiate (renderer.material.mainTexture) as Texture2D;

//	renderer.material.mainTexture = texture;
//	Debug.Log(renderer.material.texture);
}

function Update()
{
	Calculate();
}

function Calculate()
{
//	if (perlin == null)
//		perlin = new Perlin();
//	fractal = new FractalNoise(h, lacunarity, octaves, perlin);
	
	for (var y = 0;y<height;y++)
	{
		for (var x = 0;x<width;x++)
		{
//			if (gray)
//			{
//				var value = fractal.HybridMultifractal(x*scale + Time.time, y * scale + Time.time, offset);
//				texture.SetPixel(x, y, Color (value, value, value, value));
//			}
//			else
//			{
//				offsetPos = Time.time;
//				var valuex = fractal.HybridMultifractal(x*scale + offsetPos * 0.6, y*scale + offsetPos * 0.6, offset);
//				var valuey = fractal.HybridMultifractal(x*scale + 161.7 + offsetPos * 0.2, y*scale + 161.7 + offsetPos * 0.3, offset);
//				var valuez = fractal.HybridMultifractal(x*scale + 591.1 + offsetPos, y*scale + 591.1 + offsetPos * 0.1, offset);
//				texture.SetPixel(x, y, Color (valuex, valuey, valuez, 1))
;
//				var valuex =  
//				var valuey =
//				var valuez =  
//				Debug.Log(originalMaterial.mainTexure.GetPixel(x,y));
var timeModifier =  Time.time;
px = originalTexture.GetPixel(x,y+(timeModifier*10*widthSpeedModifiers[x]));
//px = new Color(px.r ,px.g ,px.b , 0.5);
				texture.SetPixel(x,y,px);
//			}
		}	
	}
	
	texture.Apply();
//	renderer.material.mainTexture = texture;
	spriteRend.sprite = Sprite.Create(texture, new Rect(0,0,width,height), pivotPt, 100f);
	
	

}