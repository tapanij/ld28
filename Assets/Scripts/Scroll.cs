﻿using UnityEngine;
using System.Collections;

public class Scroll : MonoBehaviour
{
		public bool pickedUp = false;
		public GameObject projectilePrefab;
		public bool orderSet = false;

		// Use this for initialization
		void Start ()
		{
	
		}
	
		// Update is called once per frame
		void FixedUpdate ()
		{
				// layer order while picked	
				if (!orderSet && tag == "Scroll" && pickedUp == true) {
						GetComponentInChildren<SpriteRenderer> ().sortingLayerName = "Foreground";
						orderSet = true;
				}
		}

		void OnMouseDown ()
		{
				SendMessageUpwards ("ObjectClicked", gameObject);
		}

}
