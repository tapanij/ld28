﻿using UnityEngine;
using System.Collections;

public class TextureAnimation : MonoBehaviour
{
		private int width;
		private int height;
		private Texture2D texture;
		public Texture2D originalTexture;
		private SpriteRenderer spriteRend;
		private Vector2 pivotPt;
		private float[] widthSpeedModifiers;
		public bool startTextureAnimation = false;
		private float animationTime = 0f;
		private bool audioPlayed = false;

		// Use this for initialization
		void Start ()
		{
				pivotPt = new Vector2 (0.5f, 0.5f);
				spriteRend = GetComponent<SpriteRenderer> ();

				width = originalTexture.width;
				height = originalTexture.height;
		
				widthSpeedModifiers = new float[width];
				for (var i = 0; i < width; i++) {
						widthSpeedModifiers [i] = Random.Range (1f, 3f);
				}
		
				texture = new Texture2D (originalTexture.width, originalTexture.height, TextureFormat.ARGB32, false);
				texture.filterMode = FilterMode.Point;
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (startTextureAnimation) {
						if (!audioPlayed) {
								audio.Play ();
								audioPlayed = true;
						}

						Calculate ();
				}
		}

		void Calculate ()
		{
				for (int y = 0; y<height; y++) {
						for (int x = 0; x<width; x++) {
								animationTime += Time.deltaTime;

								float timeModifier = animationTime * y * 0.0005f;

								Color px = originalTexture.GetPixel (x, (int)(y + (timeModifier * widthSpeedModifiers [x])));

								texture.SetPixel (x, y, px);

						}	
				}
		
				texture.Apply ();

				spriteRend.sprite = Sprite.Create (texture, new Rect (0, 0, width, height), pivotPt, 100f);
		}
}
