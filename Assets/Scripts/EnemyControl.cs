﻿using UnityEngine;
using System.Collections;

public class EnemyControl : MonoBehaviour
{
		public GameObject scrollPrefab;
		private float moveForce = 0.1f;
		private Vector2 moveDirection = Vector2.zero;
		private Transform player;

		// Use this for initialization
		void Start ()
		{
				player = GameObject.FindGameObjectWithTag ("Player").transform;
		}
	
		// Update is called once per frame
		void EnemyUpdate ()
		{
	
		}

		void EnemyFixedUpdate ()
		{
//				float steepness = 1f;
//				float angle = Time.time * steepness;
//				moveDirection.x = Mathf.Cos (angle);
//				moveDirection.y = Mathf.Sin (angle);
//				rigidbody2D.velocity = moveDirection * moveForce;

				// Move towards player
				Vector2 heading = new Vector2 (player.transform.position.x - transform.position.x, player.transform.position.y - transform.position.y);
				float distance = heading.magnitude;
				moveDirection = heading / distance;

				rigidbody2D.velocity = moveDirection * moveForce;
		}


		void OnDestroy ()
		{
				SendMessageUpwards ("OnEnemyReallyDestroyed");
		}
}