﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour
{
		public GameObject player;
		public GameObject inventoryPoint;
		private GameObject currentScroll;
		public GameObject fireSP;
		public GameObject iceSP;
		public GameObject iceEnemyPrefab;
		public GameObject fireEnemyPrefab;
		public GameObject thankYouGUI;
		private int enemyAmount = 10;

		// Use this for initialization
		void Start ()
		{
//				InvokeRepeating ("SpawnEnemy", 0f, 3f);
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (Input.GetKeyDown (KeyCode.Space)) {
						Application.LoadLevel (1);
				}
				if (Input.GetKeyDown (KeyCode.Escape)) {
						if (Screen.fullScreen) {
								Screen.fullScreen = false;
						}
				}

				BroadcastMessage ("PlayerUpdate");	
		}

		void FixedUpdate ()
		{	
				BroadcastMessage ("PlayerFixedUpdate");
//				BroadcastMessage ("EnemyFixedUpdate", SendMessageOptions.DontRequireReceiver);
		}

		void SpawnEnemy ()
		{
				GameObject clone = Instantiate (fireEnemyPrefab, fireSP.transform.position, Quaternion.identity) as GameObject;
				clone.transform.parent = transform;

				clone = Instantiate (iceEnemyPrefab, iceSP.transform.position, Quaternion.identity) as GameObject;
				clone.transform.parent = transform;
		}

		void ObjectClicked (GameObject go)
		{
				if (go.tag.Equals ("Scroll")) {
						// User clicked inventory, drop from it
						if (go.GetComponent<Scroll> ().pickedUp) {
								DropObject (go);
						}
						// User clicked ground scroll and it's not picked up ye
			else if (Vector3.Distance (go.transform.position, player.transform.position) < 0.4f) {
								if (currentScroll != null) {
										DropObject (currentScroll);
								}

								currentScroll = go;
								go.GetComponent<Scroll> ().pickedUp = true;
								go.transform.parent = Camera.main.transform;
								go.transform.position = inventoryPoint.transform.position;

								audio.Play ();

								BroadcastMessage ("SetProjectile", go.GetComponent<Scroll> ().projectilePrefab);
						} 
				}
		}

		void DropObject (GameObject go)
		{
				go.GetComponentInChildren<SpriteRenderer> ().sortingLayerName = "Enemy";
				go.GetComponent<Scroll> ().pickedUp = false;
				go.GetComponent<Scroll> ().orderSet = false;
				go.transform.parent = transform;
				go.transform.position = player.transform.position;
				currentScroll = null;
		}

		void ProjectileShot ()
		{
				Destroy (currentScroll);
				currentScroll = null;
		}

//		void ItemCreated (GameObject go)
//		{
//				go.transform.parent = this.transform;
//		}

		void OnEnemyReallyDestroyed ()
		{
				enemyAmount--;
				if (enemyAmount <= 0) {
						thankYouGUI.GetComponent<SpriteRenderer> ().enabled = true;
				}
		}

		void OnEnemyDestroyed (GameObject go)
		{


//				Debug.Log ("enemy destroyed");
				GameObject scrollClone = Instantiate (go.GetComponent<EnemyControl> ().scrollPrefab, new Vector3 (go.transform.position.x, go.transform.position.y + 0.1f, 0f), Quaternion.identity) as GameObject;

				scrollClone.transform.parent = transform;
		
//				ItemCreated (scrollClone);
		}
}
