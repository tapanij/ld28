﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour
{
		public string type = "projectile";
//		private Vector3 startPos = Vector3.zero;
		private float startDistance = 0f;
		public Vector3 targetPos = Vector3.zero;
		private float previousOffset = 0;
		public GameObject childObj;
		public SpriteRenderer flash;

		// Use this for initialization
		void Start ()
		{
//				startPos = transform.position;
				startDistance = Vector3.Distance (transform.position, targetPos);

				if (type == "bomb") {
						collider2D.enabled = false;
				}
				Destroy (gameObject, 2f);
		}
	
		// Update is called once per frame
		void Update ()
		{
				if (childObj && type == "bomb") {
						float distance = Vector3.Distance (transform.position, targetPos);
						if (childObj) {								

								float positionInSin = (distance / startDistance); 

								positionInSin *= 3.14f; 

								float offsetY = Mathf.Sin (positionInSin);


								float offsetChange = previousOffset - offsetY;

//						rigidbody2D.AddForce (Vector2.right * offsetChange * 100f);

//						Vector3 newPos = transform.position;
//						newPos.y += offsetChange;

								Vector3 newPos = new Vector3 (0f, offsetY, 0f);	

								childObj.transform.localPosition = newPos;

//						Vector2 newVelo = new Vector2 (rigidbody2D.velocity.x + offsetChange, rigidbody2D.velocity.y + offsetChange);		
//						rigidbody2D.velocity.x += offsetChange;
//						rigidbody2D.velocity.y += offsetChange;
//						rigidbody2D.velocity = newVelo;
						}

						if (distance < 0.2f) {
								flash.enabled = true;
								Invoke ("DisableFlash", 0.1f);
								EnableCollider ();
								rigidbody2D.Sleep ();
								Destroy (gameObject, 1f);
								Destroy (childObj);
	
								audio.Play ();
						}
				}
		}

		void DisableFlash ()
		{
				flash.enabled = false;
		}

		void EnableCollider ()
		{
				collider2D.enabled = true;
		}

		void OnTriggerEnter2D (Collider2D col)
		{
				if (col.tag != tag && (col.tag == "Fire" || col.tag == "Ice")) {
						col.gameObject.GetComponentInChildren <TextureAnimation> ().startTextureAnimation = true;

						// Destroy BOTH colliders
						Destroy (col.gameObject.GetComponent<BoxCollider2D> ());
						Destroy (col.gameObject.GetComponent<CircleCollider2D> ());

						Destroy (col.gameObject, 5f);

			
						// Call the explosion instantiation.
						//						OnExplode ();
			
//						Destroy (gameObject);

						SendMessageUpwards ("OnEnemyDestroyed", col.gameObject);

				}
		}
}
