﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour
{
		[HideInInspector]
		public bool
				facingRight = true;			// For determining which way the player is currently facing.
		public float moveForce = 0.50f;
		public float maxSpeed = 0.1f;
		public float minDistance = 0.2f;
		public GameObject projectilePrefab;
		public float projectileSpeed = 1f;
		public float animSpeedModifier = 2f;
		private Animator anim;
		public AudioSource shootAudio;

		void Awake ()
		{
				anim = GetComponent<Animator> ();
		}

		// Use this for initialization
		void Start ()
		{
	
		}
	
		// Update is called once per frame
		void PlayerUpdate ()
		{
				// MOUSE2
				if (projectilePrefab && Input.GetMouseButtonDown (1)) {
						Vector2 mousePos = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
						Ray ray = Camera.main.ScreenPointToRay (mousePos);
						Vector2 targetPos = new Vector2 (ray.origin.x, ray.origin.y);
			
						Vector2 heading = targetPos - new Vector2 (transform.position.x, transform.position.y);
						float distance = heading.magnitude;
						Vector2 direction = heading / distance;
			
						GameObject projectile = Instantiate (projectilePrefab, transform.position, Quaternion.identity) as GameObject;
						shootAudio.Play ();

						projectile.GetComponent<Projectile> ().targetPos = new Vector3 (ray.origin.x, ray.origin.y, 0);
			
						projectile.transform.parent = transform.parent;
			
						projectile.rigidbody2D.velocity = direction * projectileSpeed;
			
						projectilePrefab = null;
						SendMessageUpwards ("ProjectileShot");
			
						// If the input is moving the player right and the player is facing left...
						if (direction.x > 0 && !facingRight) {
								// ... flip the player.
								Flip ();
						}
			
			// Otherwise direction the input is moving the player left and the player is facing right...
			else if (direction.x < 0 && facingRight) {
								// ... flip the player.
								Flip ();
						}
				}
		}

		void PlayerFixedUpdate ()
		{



				// MOUSE1

				Vector2 moveDirection = Vector2.zero;

				if (Input.GetMouseButton (0)) {
						Vector2 mousePos = new Vector2 (Input.mousePosition.x, Input.mousePosition.y);
						Ray ray = Camera.main.ScreenPointToRay (mousePos);
						Vector2 targetPos = new Vector2 (ray.origin.x, ray.origin.y);

						Vector2 heading = targetPos - new Vector2 (transform.position.x, transform.position.y);
						float distance = heading.magnitude;
						if (distance >= minDistance) {
								moveDirection = heading / distance;

								// If the input is moving the player right and the player is facing left...
								if (moveDirection.x > 0 && !facingRight) {
										// ... flip the player.
										Flip ();
								}
				
								// Otherwise if the input is moving the player left and the player is facing right...
								else if (moveDirection.x < 0 && facingRight) {
										// ... flip the player.
										Flip ();
								}

						}
				}

				Vector2 newVelocity = Vector2.zero;

				if (moveDirection != Vector2.zero) {
						rigidbody2D.velocity = moveDirection * moveForce;

//						if (rigidbody2D.velocity.magnitude < maxSpeed) {
//								rigidbody2D.AddForce (moveDirection * moveForce);
//						}				
//						Debug.Log (Mathf.Abs (rigidbody2D.velocity.x));
						// If the player's horizontal velocity is greater than the maxSpeed...
//						if (Mathf.Abs (rigidbody2D.velocity.x) > maxSpeed) {
////								Debug.Log ("Mathf.Abs (rigidbody2D.velocity.x) > maxSpeed");
////								Debug.Log (Mathf.Abs (rigidbody2D.velocity.x));
//								// ... set the player's velocity to the maxSpeed in the x axis.
//								newVelocity.x = Mathf.Sign (moveDirection.x) * maxSpeed;
//								newVelocity.y = rigidbody2D.velocity.y;
//						}
//						// If the player's vertical velocity is greater than the maxSpeed...
//						if (Mathf.Abs (rigidbody2D.velocity.y) > maxSpeed) {
//								// ... set the player's velocity to the maxSpeed in the x axis.
//								newVelocity.y = Mathf.Sign (moveDirection.y) * maxSpeed;
//								newVelocity.x = rigidbody2D.velocity.x;
//						}
				}

//				rigidbody2D.velocity = newVelocity;


		
		
				// The Speed animator parameter is set to the absolute value of the horizontal input.
//				anim.SetFloat ("Speed", Mathf.Abs (h));

				// AUDIO
				audio.pitch = rigidbody2D.velocity.magnitude;
			
				// ANIMATION
				anim.SetFloat ("Speed", rigidbody2D.velocity.magnitude);
				anim.speed = rigidbody2D.velocity.magnitude * animSpeedModifier;
		
		}

		void Flip ()
		{
				// Switch the way the player is labelled as facing.
				facingRight = !facingRight;
		
				// Multiply the player's x local scale by -1.
				Vector3 theScale = transform.localScale;
				theScale.x *= -1;
				transform.localScale = theScale;
		}

		void SetProjectile (GameObject go)
		{
				projectilePrefab = go;
		}
}